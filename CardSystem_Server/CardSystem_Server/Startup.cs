﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardSystem_Server.Logic;
using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Repository;
using CardSystem_Server.Repository.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CardSystem_Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddTransient<ICreateCardRepository, CreateCardRepository>();
            services.AddTransient<ICreateCardLogic, CreateCardLogic>();
            services.AddTransient<IPutMoneyRepository, PutMoneyRepository>();
            services.AddTransient<IPutMoneyLogic, PutMoneyLogic>();
            services.AddTransient<IPassRepository, PassRepository>();
            services.AddTransient<IPassLogic, PassLogic>();
            services.AddTransient<IRemoveRepository, RemoveRepository>();
            services.AddTransient<IRemoveLogic, RemoveLogic>();

            string connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            services.AddDbContext<Database>(options => options.UseSqlServer(connectionString));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
