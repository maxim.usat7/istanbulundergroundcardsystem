﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace CardSystem_Server.Models
{
    public class CardModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        public int money { get; set; }

        public CardModel(int money)
        {
            this.money = money;
        }

        public CardModel()
        {

        }

        public CardModel(long id)
        {
            this.id = id;
        }

        public CardModel(int money, long id)
        {
            this.money = money;
            this.id = id;
        }
    }
}
