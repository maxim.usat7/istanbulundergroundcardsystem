﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Models
{
    public class AdminModel
    {
        public string password { get; set; }
        public long cardId { get; set; }

        public AdminModel(string password, long id)
        {
            this.password = password;
            this.cardId = cardId;
        }

        public AdminModel(string password)
        {
            this.password = password;
        }

        public AdminModel()
        {

        }
    }
}
