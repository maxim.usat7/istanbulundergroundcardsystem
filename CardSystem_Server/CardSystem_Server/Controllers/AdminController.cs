﻿using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Controllers
{
    [ApiController]
    public class AdminController : ControllerBase
    {
        public AdminController(IRemoveLogic logic)
        {
            this.logic = logic;
        }

        public static bool IsCardSystemWork = true;
        private static string adminPassword = "AMC1995G5SFTSRV101PGT0";
        private IRemoveLogic logic;

        [HttpPost]
        [Route("api/remove")]
        public IActionResult RemoveCard([FromBody]AdminModel model)
        {
            if(model.password == adminPassword)
            {
                var answer = logic.Remove(model.cardId);
                switch (answer)
                {
                    case Logic.RemoveLogic.RemoveAnswers.Removed:
                        return Ok("Removed");
                    case Logic.RemoveLogic.RemoveAnswers.ServerError:
                        return StatusCode(500, "Server error");
                    case Logic.RemoveLogic.RemoveAnswers.WrongId:
                        return BadRequest("Wrong card id");
                    default:
                        return StatusCode(500, "Server error");
                }
            }
            else
            {
                return BadRequest("Wrong password");
            }
        }

        [HttpPost]
        [Route("api/stop")]
        public IActionResult StopSystem([FromBody]AdminModel model)
        {
            if(model.password == adminPassword)
            {
                IsCardSystemWork = false;
                return Ok("Card system stopped");
            }
            else
            {
                return BadRequest("Wrong admin password");
            }
        }

        [HttpPost]
        [Route("api/start")]
        public IActionResult StartSystem([FromBody]AdminModel model)
        {
            if (model.password == adminPassword)
            {
                IsCardSystemWork = true;
                return Ok("Card system work");
            }
            else
            {
                return BadRequest("Wrong admin password");
            }
        }
    }
}
