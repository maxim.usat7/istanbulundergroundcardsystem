﻿using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CardSystem_Server.Logic.PassLogic;

namespace CardSystem_Server.Controllers
{
    [ApiController]
    public class PassController : ControllerBase
    {
        private IPassLogic logic;

        public PassController(IPassLogic logic)
        {
            this.logic = logic;
        }

        [HttpPost]
        [Route("api/pass")]
        public IActionResult Pass([FromBody] CardModel card)
        {
            PassAnswers answer = logic.Pass(card);

            switch (answer)
            {
                case PassAnswers.ServerError:
                    return StatusCode(500, "Server error");
                case PassAnswers.Go:
                    return Ok("Go");
                case PassAnswers.NoMoney:
                    return BadRequest("Not enough money on card");
                case PassAnswers.WrongId:
                    return BadRequest("Wrong id");
                default:
                    return StatusCode(500, "Server error");
            }
        }
    }
}
