﻿using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Models;
using CardSystem_Server.Repository.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CardSystem_Server.Logic.CreateCardLogic;

namespace CardSystem_Server.Controllers
{
    [ApiController]
    public class CreateCardController : ControllerBase
    {
        private ICreateCardLogic logic;

        public CreateCardController(ICreateCardLogic logic)
        {
            this.logic = logic;
        }

        [HttpPost]
        [Route("api/createcard")]
        public IActionResult Create([FromBody] CardModel card)
        {
            (CreateCardAnswers, long?) answer = logic.CreateCard(card);

            switch (answer.Item1)
            {
                case CreateCardAnswers.Created:
                    return Created("", answer.Item2);
                case CreateCardAnswers.ServerError:
                    return StatusCode(500, "Server error");
                default:
                    return StatusCode(500, "Server error");
            }
        }
    }
}
