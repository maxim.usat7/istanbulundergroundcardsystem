﻿using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CardSystem_Server.Logic.PutMoneyLogic;

namespace CardSystem_Server.Controllers
{
    [ApiController]
    public class PutMoneyController : ControllerBase
    {
        private IPutMoneyLogic logic;

        public PutMoneyController(IPutMoneyLogic logic)
        {
            this.logic = logic;
        }

        [HttpPost]
        [Route("api/putmoney")]
        public IActionResult Put([FromBody] CardModel card)
        {
            PutMoneyAnswers answer = logic.PutMoney(card);

            switch (answer)
            {
                case PutMoneyAnswers.ServerError:
                    return StatusCode(500, "Server error");
                case PutMoneyAnswers.Done:
                    return Ok("Money added");
                default:
                    return StatusCode(500, "Server error");
            }
        }
    }
}
