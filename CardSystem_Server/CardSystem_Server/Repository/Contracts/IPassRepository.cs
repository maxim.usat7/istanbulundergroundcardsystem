﻿using CardSystem_Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Repository.Contracts
{
    public interface IPassRepository
    {
        int? GetMoneyFromCard(CardModel card);
        void UpdateMoneyOnCard(CardModel card);
    }
}
