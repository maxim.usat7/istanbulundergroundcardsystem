﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Repository.Contracts
{
    public interface IRemoveRepository
    {
        bool DeleteCard(long id);
    }
}
