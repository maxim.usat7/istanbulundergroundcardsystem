﻿using CardSystem_Server.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Repository
{
    public class Database : DbContext
    {
        public DbSet<CardModel> cards { get; set; }

        public Database(DbContextOptions<Database> opts) : base(opts)
        {

        }

        //protected override void OnConfiguring(DbContextOptionsBuilder builder)
        //{
        //    builder.UseSqlServer("Server = (localdb)\\mssqllocaldb; Database = underground; Trusted_Connection = True;");
        //}
    }
}
