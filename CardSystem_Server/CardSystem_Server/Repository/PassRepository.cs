﻿using CardSystem_Server.Models;
using CardSystem_Server.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Repository
{
    public class PassRepository : IPassRepository
    {
        private Database db;

        public PassRepository(Database db)
        {
            this.db = db;
        }

        public int? GetMoneyFromCard(CardModel card)
        {
            CardModel dbCard = db.cards.FirstOrDefault(c => c.id == card.id);

            if (dbCard is null)
            {
                return null;
            }
            else
            {
                int? money = dbCard.money;
                return money;
            }
        }

        public void UpdateMoneyOnCard(CardModel card)
        {
                db.cards.Update(card);
                db.SaveChanges();
        }
    }
}
