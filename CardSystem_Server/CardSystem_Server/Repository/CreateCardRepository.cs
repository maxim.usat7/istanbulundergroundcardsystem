﻿using CardSystem_Server.Models;
using CardSystem_Server.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Repository
{
    public class CreateCardRepository : ICreateCardRepository
    {
        private Database db;

        public CreateCardRepository(Database db)
        {
            this.db = db;
        }

        public long CreateCard(CardModel card)
        {
                db.cards.Add(card);
                db.SaveChanges();

                long id = db.cards.Last().id;
                return id;
        }
    }
}
