﻿using CardSystem_Server.Models;
using CardSystem_Server.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Repository
{
    public class PutMoneyRepository : IPutMoneyRepository
    {
        private Database db;

        public PutMoneyRepository(Database db)
        {
            this.db = db;
        }

        public void UpdateMoneyOnCard(CardModel card)
        {
                db.cards.Update(card);
                db.SaveChanges();
        }
    }
}
