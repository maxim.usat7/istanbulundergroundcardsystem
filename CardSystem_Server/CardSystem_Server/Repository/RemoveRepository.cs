﻿using CardSystem_Server.Models;
using CardSystem_Server.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Repository
{
    public class RemoveRepository : IRemoveRepository
    {
        private Database db;

        public RemoveRepository(Database db)
        {
            this.db = db;
        }

        public bool DeleteCard(long id)
        {
            CardModel card = db.cards.FirstOrDefault(dbCard => dbCard.id == id);
            if(card is null)
            {
                return false;
            }
            else
            {
                db.cards.Remove(card);
                db.SaveChanges();
                return true;
            }
        }
    }
}
