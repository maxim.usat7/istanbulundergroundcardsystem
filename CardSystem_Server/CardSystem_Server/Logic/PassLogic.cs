﻿using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Models;
using CardSystem_Server.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Logic
{
    public class PassLogic : IPassLogic
    {
        private IPassRepository repository;

        public PassLogic(IPassRepository repository)
        {
            this.repository = repository;
        }

        public enum PassAnswers
        {
            ServerError,
            Go,
            NoMoney,
            WrongId
        }

        public PassAnswers Pass(CardModel card)
        {
            int? money;
            card.money = 0;
            try
            {
                money = repository.GetMoneyFromCard(card);
            }
            catch (Exception e)
            {
                return PassAnswers.ServerError;
            }

            if (money is null)
            {
                return PassAnswers.WrongId;
            }

            if(money > 2)
            {
                card.money -= 2;

                try
                {
                    repository.UpdateMoneyOnCard(card);
                    return PassAnswers.Go;
                }
                catch (Exception)
                {
                    return PassAnswers.ServerError;
                }
            }
            else
            {
                return PassAnswers.NoMoney;
            }
        }
    }
}
