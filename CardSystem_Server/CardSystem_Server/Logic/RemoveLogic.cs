﻿using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Logic
{
    public class RemoveLogic : IRemoveLogic
    {
        private IRemoveRepository repository;

        public RemoveLogic(IRemoveRepository repository)
        {
            this.repository = repository;
        }

        public enum RemoveAnswers
        {
            Removed,
            ServerError,
            WrongId
        }

        public RemoveAnswers Remove(long id)
        {
            try
            {
                bool result = repository.DeleteCard(id);

                if(result is true)
                {
                    return RemoveAnswers.Removed;
                }
                else
                {
                    return RemoveAnswers.WrongId;
                }
            }
            catch (Exception)
            {
                return RemoveAnswers.ServerError;
            }
        }
    }
}
