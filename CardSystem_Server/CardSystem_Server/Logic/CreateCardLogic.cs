﻿using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Models;
using CardSystem_Server.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Logic
{
    public class CreateCardLogic : ICreateCardLogic
    {
        private ICreateCardRepository reposiory;

        public enum CreateCardAnswers
        {
            Created,
            ServerError
        }

        public CreateCardLogic(ICreateCardRepository repository)
        {
            this.reposiory = repository;
        }

        public (CreateCardAnswers, long?) CreateCard(CardModel card)
        {
            try
            {
                long id = reposiory.CreateCard(card);
                return (CreateCardAnswers.Created, id);
            }
            catch (Exception)
            {
                return (CreateCardAnswers.ServerError, null);
            }
        }
    }
}
