﻿using CardSystem_Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CardSystem_Server.Logic.CreateCardLogic;

namespace CardSystem_Server.Logic.Contracts
{
    public interface ICreateCardLogic
    {
        (CreateCardAnswers, long?) CreateCard(CardModel card);
    }
}
