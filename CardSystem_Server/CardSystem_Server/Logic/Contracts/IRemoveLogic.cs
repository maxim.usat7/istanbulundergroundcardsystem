﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CardSystem_Server.Logic.RemoveLogic;

namespace CardSystem_Server.Logic.Contracts
{
    public interface IRemoveLogic
    {
        RemoveAnswers Remove(long id);
    }
}
