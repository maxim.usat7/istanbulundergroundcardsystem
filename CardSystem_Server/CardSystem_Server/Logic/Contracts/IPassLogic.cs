﻿using CardSystem_Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CardSystem_Server.Logic.PassLogic;

namespace CardSystem_Server.Logic.Contracts
{
    public interface IPassLogic
    {
        PassAnswers Pass(CardModel card);
    }
}
