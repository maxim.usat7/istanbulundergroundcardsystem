﻿using CardSystem_Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CardSystem_Server.Logic.PutMoneyLogic;

namespace CardSystem_Server.Logic.Contracts
{
    public interface IPutMoneyLogic
    {
        PutMoneyAnswers PutMoney(CardModel card);
    }
}
