﻿using CardSystem_Server.Logic.Contracts;
using CardSystem_Server.Models;
using CardSystem_Server.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardSystem_Server.Logic
{
    public class PutMoneyLogic : IPutMoneyLogic
    {
        private IPutMoneyRepository repository;

        public enum PutMoneyAnswers
        {
            ServerError,
            Done
        }

        public PutMoneyLogic(IPutMoneyRepository repository)
        {
            this.repository = repository;
        }

        public PutMoneyAnswers PutMoney(CardModel card)
        {
            try
            {
                repository.UpdateMoneyOnCard(card);
                return PutMoneyAnswers.Done;
            }
            catch (Exception)
            {
                return PutMoneyAnswers.ServerError;
            }
        }
    }
}
